<?php
class Order{
    private $id;
    private $proID;
    private $userID;
    private $status;
    private $orderDate;

    public function getId(){
        return $this->id;
    }
    public function setId($id){
        $this->id = $id;
    }

    public function getProID(){
        return $this->proID;
    }
    public function setProID($proID){
        $this->proID = $proID;
    }

    public function getUserID(){
        return $this->userID;
    }
    public function setUserID($userID){
        $this->userID = $userID;
    }

    public function getStatus(){
        return $this->status;
    }
    public function setStatus($status){
        $this->status = $status;
    }

    public function getOrderDate(){
        return $this->orderDate;
    }
    public function setOrderDate($orderDate){
        $this->orderDate = $orderDate;
    }
}