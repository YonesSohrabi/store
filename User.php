<?php

class User
{
    private $id;
    private $name;
    private $family;
    private $email;
    private $password;
    private $phone;
    private $address;

    // function __construct($name, $family, $email, $password, $id = null, $phone = null, $address = null)
    // {
    //     $this->id = $id;
    //     $this->name = $name;
    //     $this->family = $family;
    //     $this->email = $email;
    //     $this->password = $password;
    //     $this->phone = $phone;
    //     $this->address = $address;
    // }

    public function getId()
    {
        return $this->id;
    }
    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getFamily()
    {
        return $this->family;
    }
    public function setFamily($family)
    {
        $this->family = $family;
    }

    public function getEmail()
    {
        return $this->email;
    }
    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getPassword()
    {
        return $this->password;
    }
    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getPhone()
    {
        return $this->phone;
    }
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function getAddress()
    {
        return $this->address;
    }
    public function setAddress($address)
    {
        $this->address = $address;
    }

    public function addUser()
    {
        require_once "database.php";
        $link = connectToDB();
        $sql = 'INSERT INTO customers VALUES (:id, :namee, :family, :phone, :email,:passwordd ,:addresss)';
        $statement = $link->prepare($sql);
        $inserted = $statement->execute([
            'id' => $this->getId(),
            'namee' => $this->getName(),
            'family' => $this->getFamily(),
            'phone' => $this->getPhone(),
            'email' => $this->getEmail(),
            'passwordd' => $this->getPassword(),
            'addresss' => $this->getAddress()
        ]);
        if ($inserted) {
            echo 'Row inserted!<br>';
        }
        $link = null;
    }

    public function getInfoUser()
    {
        require_once "database.php";
        $link = connectToDB();
        $sql = 'SELECT * FROM customers WHERE id = :id';
        $statement = $link->prepare($sql);
        $id = $this->getId();
        $statement->bindParam(':id', $id, PDO::PARAM_INT);
        $statement->execute();
        $customer = $statement->fetch(PDO::FETCH_ASSOC);
        if ($customer) {
            $this->setName($customer['name']);
            $this->setFamily($customer['family']);
            $this->setEmail($customer['email']);
            $this->setPassword($customer['password']);
            $this->setPhone($customer['phone']);
            $this->setAddress($customer['address']);
            return $customer;
        }
    }


    public function deleteUser()
    {
        require_once "database.php";
        $link = connectToDB();
        $sql = 'DELETE FROM customers WHERE id = :id';
        $statement = $link->prepare($sql);
        $statement->bindParam(':id', $this->getId(), PDO::PARAM_INT);
        if ($statement->execute()) {
            echo 'customers id ' . $this->getId() . ' was deleted successfully.';
        }
    }

    public function updateUser()
    {
        require_once "database.php";
        $link = connectToDB();
        $sql = "UPDATE customers SET `name` = :fname, `family` = :lname, `email` = :email,";
        $sql .= " `phone` = :phone, `password` = :pass, `address` = :address WHERE id = :id";
        $statement = $link->prepare($sql);
        $res = $statement->execute([
            'id' => $this->getId(),
            'fname' => $this->getName(),
            'lname' => $this->getFamily(),
            'phone' => $this->getPhone(),
            'email' => $this->getEmail(),
            'pass' => $this->getPassword(),
            'address' => $this->getAddress()
        ]);
    }
}
// $user = new User();
// $user->setId(6);
// $user->setName("علی");
// $user->setFamily("سهرابی");
// $user->setEmail("y.sohrabi959@gmail.com");
// $user->setPassword("12345");
// $user->setPhone("09180042743");
// $user->setAddress("همدان");
// $user->updateUser();
