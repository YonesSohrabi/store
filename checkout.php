
<?php
session_start();
if ($_SESSION["login"] != "user") {
  header("Location: http://localhost/hw/9/signin.php");
}
include_once "database.php";
$product_info_arr = getAllPeroduct();

?>

<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
  <link rel="stylesheet" href="assets/css/style.css" />
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" />

  <title>لیست خرید</title>
</head>

<body>

  <nav class="navbar navbar-expand-lg navbar-light bg-light">

    <div class="collapse navbar-collapse d-flex justify-content-between" id="navbarNavAltMarkup">
      <div class="navbar">
        <div><span class="nav-item nav-link text-dark fs-4 me-3 active">فروشگاه</span></div>
        <div><a class="nav-item nav-link text-dark" href="index.php">محصولات</a></div>
      </div>
      <div class="navbar-nav">
        <a class="nav-item nav-link active">
          سبد خرید
          <span class="position-relative top-0 start-100 translate-middle-y badge rounded-pill bg-danger" id="total_items">
            0
          </span>
        </a>
        <a class="nav-item nav-link active" href="profile.php">
          پروفایل
        </a>
        <a class="nav-item nav-link ms-5" href="#" id="exit">خروج</a>
        <div class="mt-2 ms-5">
          <?php
          $timeOnline = time() - $_COOKIE["timeLogin"];
          if ($timeOnline < 60) {
            echo "زمان حضور شما : " . $timeOnline . " ثانیه ";
          } else {
            echo ("زمان حضور شما : " . (int)($timeOnline / 60) . " دقیقه و " . ($timeOnline % 60) . " ثانیه ");
          }
          ?>
        </div>
      </div>
    </div>
  </nav>


  <div class="container-fluid">
    <div class="row">
      <aside class="col-lg-9">
        <div class="card">
          <div class="table-responsive">
            <table class="table table-borderless table-shopping-cart">
              <thead class="text-muted">
                <tr class=" text-uppercase text-center">
                  <th scope="col">محصول</th>
                  <th scope="col" width="100">تعداد</th>
                  <th scope="col" width="300">قیمت</th>
                  <th scope="col" class="text-center d-none d-md-block" width="200">
                    <button class="btn btn-light col-6" id="removeAll">حذف کل</button>
                  </th>
                </tr>
              </thead>
              <tbody id="mycart">
                <?php
                if (isset($_COOKIE['proID'])) {
                  $arrID = explode(",", $_COOKIE['proID']);
                  $sumPrice = 0;

                  foreach ($arrID as $id) {
                    $product_info = null;
                    foreach ($product_info_arr as $product) {
                      if ($product["id"] == $id) {
                        $product_info = $product;
                        $sumPrice += $product["price"];
                        break;
                      }
                    }
                ?>
                    <tr>
                      <td>
                        <figure class="itemside align-items-center">
                          <div class="ms-3 text-danger">#<span id="productID"><?php echo $product_info["id"] ?></span></div>
                          <div class="aside">
                            <img src="<?php echo $product_info["imgUrl"] ?>" class="img-sm" />
                          </div>
                          <figcaption class="info me-3">
                            <a href="#" class="title text-dark"><?php echo $product_info["name"] ?></a>
                            <p class="text-muted small">
                              برند : <?php echo $product_info["brand"] ?>
                            </p>
                          </figcaption>
                        </figure>
                      </td>
                      <td>
                        <select class="form-control">
                          <option>1</option>
                        </select>
                      </td>
                      <td>
                        <div class="price-wrap text-center">
                          <var class="price strike-text"><?php echo $product_info["price"] ?></var>
                          <small class="text-muted"> <?php echo $product_info["price"] * (90 / 100) ?> با تخفیف </small>
                        </div>
                      </td>
                      <td class="text-center none d-md-block">
                        <button class="btn btn-light" onclick="deleteRow(this);">حذف</button>
                      </td>
                    </tr>
                <?php }
                } ?>
              </tbody>
            </table>
          </div>
        </div>
      </aside>
      <aside class="col-lg-3">
        <div class="card mb-3">
          <div class="card-body">
            <form>
              <div class="form-group">
                <label>کد تخفیف داری ؟</label>
                <div class="input-group">
                  <input type="text" class="form-control coupon" name="" placeholder="کد تخفیف" />
                  <span class="input-group-append d-flex">
                    <button class="btn btn-primary rounded btn-apply coupon">
                      وارد کنید
                    </button>
                  </span>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="card position-sticky sticky-top">
          <div class="card-body">
            <dl class="dlist-align">
              <dt>جمع قیمت :</dt>
              <dd class="text-right ml-3"><?php echo @$sumPrice ?></dd>
            </dl>
            <dl class="dlist-align">
              <dt>تخفیف :</dt>
              <dd class="text-right text-danger ml-3"><?php echo @$sumPrice * (10 / 100) ?></dd>
            </dl>
            <dl class="dlist-align">
              <dt>قیمت نهایی :</dt>
              <dd class="text-right text-dark b ml-3">
                <strong><?php echo @$sumPrice * (90 / 100) ?></strong>
              </dd>
            </dl>
            <hr />
            <a href="#" class="btn btn-out btn-primary btn-square btn-main">
              پرداخت سبد خرید
            </a>
            <a href="#" class="btn btn-out btn-success btn-square btn-main mt-2">ادامه خرید</a>
          </div>
        </div>
      </aside>
    </div>
  </div>
  <!-- jQuery -->
  <script src="assets/js/jquery-3.6.0.min.js"></script>

  <!-- Bootstrap -->
  <script src="assets/js/bootstrap.min.js"></script>

  <!-- Font Awesome -->
  <script defer src="assets/js/all.min.js"></script>

  <!-- Chart JS -->
  <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

  <script src="assets/js/script.js"></script>
</body>

</html>