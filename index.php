<?php
session_start();
if ($_SESSION["login"] != "user") {
  header("Location: http://localhost/hw/9/signin.php");
}
include_once "database.php";
$product_info_arr = getAllPeroduct();
$discount = 10;
?>




<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
  <link rel="stylesheet" href="assets/css/style.css" />
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" />
  <title>محصولات</title>
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">

    <div class="collapse navbar-collapse d-flex justify-content-between" id="navbarNavAltMarkup">
      <div class="navbar">
        <div><span class="nav-item nav-link text-dark fs-4 me-3 active">فروشگاه</span></div>
        <div><a class="nav-item nav-link text-dark">محصولات</a></div>
      </div>
      <div class="navbar-nav">
        <a class="nav-item nav-link active" href="checkout.php">
          سبد خرید
          <span class="position-relative top-0 start-100 translate-middle-y badge rounded-pill bg-danger" id="total_items">
            0
          </span>
        </a>
        <a class="nav-item nav-link active" href="profile.php">
          پروفایل
        </a>
        <a class="nav-item nav-link ms-5" href="#" id="exit">خروج</a>
        <div class="mt-2 ms-5">
          <?php
          $timeOnline = time() - $_COOKIE["timeLogin"];
          if ($timeOnline < 60) {
            echo "زمان حضور شما : " . $timeOnline . " ثانیه ";
          } else {
            echo ("زمان حضور شما : " . (int)($timeOnline / 60) . " دقیقه و " . ($timeOnline % 60) . " ثانیه ");
          }
          ?>
        </div>
      </div>
    </div>
  </nav>
  <div class="container-fluid">
    <div class="row">
      <div class="container mt-3 mb-5">
        <div class="d-flex justify-content-center row">
          <div class="col-md-10">
            <form action="" method="post">
              <?php foreach ($product_info_arr as $product) { ?>
                <div class="row p-2 bg-white border rounded mt-2">
                  <div class="col-md-3 mt-1">
                    <img class="img-fluid img-responsive rounded product-image" id="<?php echo $product["id"] ?>_img" src="<?php echo $product["imgUrl"] ?>" />
                  </div>
                  <div class="col-md-6 mt-1">
                    <h5><?php echo $product["name"] ?> <span class="product-id"><?php echo $product["id"] ?>#</span></h5>
                    <div class="mt-1 mb-1 spec-1">

                      <?php
                      $tags = explode("-", $product["tags"]);
                      foreach ($tags as $tag) { ?>

                        <label class="btn btn-secondary text-light mb-1"><?php echo $tag ?></label>
                      <?php } ?>
                    </div>
                    <p class="text-justify text-truncate para mb-0">
                      <?php echo $product["description"] ?>
                  </div>
                  <div class="
                    align-items-center align-content-center
                    col-md-3
                    border-left
                    mt-1
                  ">
                    <div class="d-flex flex-row align-items-center">
                      <h4 class="ms-1" id="<?php echo $product["id"] ?>_priceDis"><?php echo (+$product["price"]) * ((100 - $discount) / 100) ?></h4>
                      <span class="strike-text" id="<?php echo $product["id"] ?>_price"><?php echo $product["price"] ?></span>
                    </div>
                    <h6 class="text-success">ارسال رایگان</h6>
                    <div class="d-flex flex-column mt-4">
                      <button class="btn btn-primary btn-sm" type="button">جزئیات</button>
                      <?php $idPro = $product["id"] ?>
                      <button class="btn btn-outline-primary btn-sm mt-2" type="button" onclick="cart('<?php echo $idPro ?>')">
                        اضافه کردن به سبد خرید
                      </button>
                    </div>
                  </div>
                </div>
              <?php } ?>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- jQuery -->
  <script src="assets/js/jquery-3.6.0.min.js"></script>

  <!-- Bootstrap -->
  <script src="assets/js/bootstrap.min.js"></script>

  <!-- Font Awesome -->
  <script defer src="assets/js/all.min.js"></script>

  <script src="assets/js/script.js"></script>
</body>

</html>