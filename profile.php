<?php
require_once "User.php";
session_start();
if ($_SESSION["login"] != "user") {
    header("Location: http://localhost/hw/9/signin.php");
}
$id = $_SESSION["userID"];

$user = new User();
$user->setId($id);
$infoUser = $user->getInfoUser();

?>

<!DOCTYPE html>
<html lang="fa" dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <style>
        body {
            background-color: #f9f9fa
        }

        .padding {
            padding: 3rem !important
        }

        .user-card-full {
            overflow: hidden
        }

        .card {
            border-radius: 5px;
            box-shadow: 0 1px 20px 0 rgba(69, 90, 100, 0.08);
            border: none;
            margin-bottom: 30px
        }

        .m-r-0 {
            margin-right: 0px
        }

        .m-l-0 {
            margin-left: 0px
        }

        .user-card-full .user-profile {
            border-radius: 5px 0 0 5px
        }

        .bg-c-lite-green {
            background: linear-gradient(to right, #ee5a6f, #f29263)
        }

        .user-profile {
            padding: 20px 0
        }

        .card-block {
            padding: 1.25rem
        }

        .m-b-25 {
            margin-bottom: 25px
        }

        .img-radius {
            border-radius: 5px
        }

        h6 {
            font-size: 14px
        }

        .card .card-block p {
            line-height: 25px
        }

        @media only screen and (min-width: 1400px) {
            p {
                font-size: 14px
            }
        }

        .card-block {
            padding: 1.25rem
        }

        .b-b-default {
            border-bottom: 1px solid #e0e0e0
        }

        .m-b-20 {
            margin-bottom: 20px
        }

        .p-b-5 {
            padding-bottom: 5px !important
        }

        .card .card-block p {
            line-height: 25px
        }

        .m-b-10 {
            margin-bottom: 10px
        }

        .text-muted {
            color: #919aa3 !important
        }

        .b-b-default {
            border-bottom: 1px solid #e0e0e0
        }

        .f-w-600 {
            font-weight: 600
        }

        .m-b-20 {
            margin-bottom: 20px
        }

        .m-t-40 {
            margin-top: 20px
        }

        .p-b-5 {
            padding-bottom: 5px !important
        }

        .m-b-10 {
            margin-bottom: 10px
        }

        .m-t-40 {
            margin-top: 20px
        }

        .user-card-full .social-link li {
            display: inline-block
        }

        .user-card-full .social-link li a {
            font-size: 20px;
            margin: 0 10px 0 0;
            transition: all 0.3s ease-in-out
        }
    </style>
    <title>Profile</title>
</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">

        <div class="collapse navbar-collapse d-flex justify-content-between" id="navbarNavAltMarkup">
            <div class="navbar">
                <div><span class="nav-item nav-link text-dark fs-4 me-3 active">فروشگاه</span></div>
                <div><a class="nav-item nav-link text-dark" href="index.php">محصولات</a></div>
            </div>
            <div class="navbar-nav">
                <a class="nav-item nav-link active" href="checkout.php">
                    سبد خرید
                    <span class="position-relative top-0 start-100 translate-middle-y badge rounded-pill bg-danger" id="total_items">
                        0
                    </span>
                </a>
                <a class="nav-item nav-link active">
                    پروفایل
                </a>
                <a class="nav-item nav-link ms-5" href="#" id="exit">خروج</a>
                <div class="mt-2 ms-5">
                    <?php
                    $timeOnline = time() - $_COOKIE["timeLogin"];
                    if ($timeOnline < 60) {
                        echo "زمان حضور شما : " . $timeOnline . " ثانیه ";
                    } else {
                        echo ("زمان حضور شما : " . (int)($timeOnline / 60) . " دقیقه و " . ($timeOnline % 60) . " ثانیه ");
                    }
                    ?>
                </div>
            </div>
        </div>
    </nav>
    <div class="page-content page-container" id="page-content">
        <div class="padding">
            <div class="row container d-flex justify-content-center">
                <div class="col-xl-8 col-md-12">
                    <div class="card user-card-full">
                        <div class="row m-l-0 m-r-0">
                            <div class="col-sm-4 bg-c-lite-green user-profile">
                                <div class="card-block text-center text-white">
                                    <div class="m-b-25"> <img src="https://img.icons8.com/bubbles/100/000000/user.png" class="img-radius" alt="User-Profile-Image"> </div>
                                    <h6 class="f-w-600"><?php echo $infoUser['name'].' '.$infoUser['family'] ?></h6>
                                    <p>طراح وب</p>
                                    <p>شناسه کاربری : <span id="idCustomer"><?php echo $infoUser['id'] ?></span></p>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="card-block">
                                    <h6 class="m-b-20 p-b-5 b-b-default f-w-600">اطلاعات</h6>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p class="m-b-10 f-w-600">ایمیل</p>
                                            <input type="text" class="text-muted f-w-400 form-control" value="<?php echo $infoUser['email'] ?>" id="newEmail">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="m-b-10 f-w-600">رمز عبور</p>
                                            <input type="text" class="text-muted f-w-400 form-control" value="<?php echo $infoUser['password'] ?>" id="newPass">
                                        </div>
                                    </div>
                                    <h6 class="m-b-20 m-t-40 p-b-5 b-b-default f-w-600">بیوگرافی</h6>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p class="m-b-10 f-w-600">نام</p>
                                            <input type="text" class="text-muted f-w-400 form-control" value="<?php echo $infoUser['name'] ?>" id="newFname">
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="m-b-10 f-w-600">نام خانوادگی</p>
                                            <input type="text" class="text-muted f-w-400 form-control" value="<?php echo $infoUser['family'] ?>" id="newLname">
                                        </div>
                                        <div class="col-sm-12">
                                            <p class="m-b-10 f-w-600">شماره همراه</p>
                                            <input type="text" class="text-muted f-w-400 form-control" value="<?php echo $infoUser['phone'] ?>" id="newPhone">
                                        </div>
                                        <div class="col-12">
                                            <p class="m-b-10 mt-2 f-w-600">آدرس</p>
                                            <input type="text" class="text-muted f-w-400 form-control" value="<?php echo $infoUser['address'] ?>" id="newAddress">
                                        </div>
                                        <div class="col-12 mt-3">
                                            <button class="col-12 btn btn-warning" onclick="editProfUser()">ویرایش</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery -->
    <script src="assets/js/jquery-3.6.0.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Font Awesome -->
    <script defer src="assets/js/all.min.js"></script>

    <script src="assets/js/script.js"></script>
</body>

</html>