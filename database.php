<?php
include_once "User.php";

function connectToDB()
{
    $dbname = 'store';
    $dbuser = 'root';
    $dbpass = '';
    $dsn = "mysql:host=localhost:3307;dbname=$dbname";
    try {
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $conn = new PDO($dsn, $dbuser, $dbpass, $options);
        // if ($conn) {
        //     echo "Connected to database successfully!";
        // }
    } catch (\PDOException $e) {
        throw new \PDOException($e->getMessage(), (int)$e->getCode());
    }
    return $conn;
}

function getAllUser()
{
    $link = connectToDB();
    $sql = "SELECT * FROM customers";
    $statement = $link->prepare($sql);
    $statement->execute();
    $customer = $statement->fetchAll(PDO::FETCH_ASSOC);
    if ($customer) {
        return $customer;
    }
}

function getAllPeroduct()
{
    $link = connectToDB();
    $sql = "SELECT * FROM products";
    $statement = $link->prepare($sql);
    $statement->execute();
    $products = $statement->fetchAll(PDO::FETCH_ASSOC);
    if ($products) {
        return $products;
    }
}

if (isset($_POST["user_info_id"])) {
    $id = $_POST["user_info_id"];
    $user = new User();
    $user->setId($id);
    $infoUser = $user->getInfoUser();
    echo json_encode($infoUser);
}

if (isset($_POST["user_del_id"])) {
    $id = $_POST["user_del_id"];
    $user = new User();
    $user->setId($id);
    $user->deleteUser();
}

if (isset($_POST["user_info_upadte"])) {
    $userInfo = $_POST["user_info_upadte"];
    $user = new User();
    $user->setId($userInfo['id']);
    $user->setName($userInfo['name']);
    $user->setFamily($userInfo['family']);
    $user->setEmail($userInfo['email']);
    $user->setPassword($userInfo['password']);
    $user->setPhone($userInfo['phone']);
    $user->setAddress($userInfo['address']);
    $user->updateUser();
}