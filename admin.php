<?php
session_start();
if ($_SESSION["login"] != "admin") {
  header("Location: http://localhost/hw/9/signin.php");
}
include_once "database.php";

$usersInfo = getAllUser();

?>
<!DOCTYPE html>
<html lang="fa" dir="rtl">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
  <link rel="stylesheet" href="assets/css/style.css" />

  <style>
    body {
      background-color: #535fe6;
    }

    .container {
      padding-bottom: 20px;
      margin-top: 10px;
      border-radius: 5px;
    }


    .img-radius {
      border-radius: 50%;
    }
  </style>

  <title>پنل مدیریت</title>
</head>

<body>
  <div class="container">
    <div class="row" id="detailsCard">
      <div class="d-flex flex-row justify-content-center p-2 align-self-start">
        <div class="col-6 rounded">
          <div class="row">
            <div class="col-12">
              <div class="card user-card">
                <div class="card-block">
                  <div class="user-image">
                    <img src="assets/images/avatar.png" class="img-radius" alt="User-Profile-Image" />
                  </div>
                  <h6 class="f-w-600 m-t-25 m-b-10" id="fullName">یونس سهرابی</h6>
                  <p class="text-muted" id="textUnderFullName">
                    شماره همراه : <span id="phoneInCard"></span> | ایمیل : <span id="emailInCard"></span>
                  </p>
                  <hr />
                  <p class="text-muted m-t-15">آخرین بازدید : <span id="lastOnline">دیروز</span></p>
                  <div class="counter-block m-t-10 p-20">
                    <div class="row">
                      <div class="col-4">
                        <span>آیدی کاربر</span>
                        <p id="idCustomer">189</p>
                      </div>

                      <div class="col-4">
                        <span>تعداد سفارش های کاربر</span>
                        <p id="orderNumUser">1256</p>
                      </div>
                      <div class="col-4">
                        <span>مقدار کل خرید</span>
                        <p id="orderPriceSum">8562</p>
                      </div>
                    </div>
                  </div>
                  <p class="m-t-15 text-muted" id="customerAddress">
                    نشانی : همدان - اسدآباد
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-6 mx-1 card p-1 rounded">
          <div class="col-sm-12">

            <div class="card-block">
              <div class="row">
                <div class="col-sm-6">
                  <p class="m-b-10 f-w-600">ایمیل</p>
                  <input type="text" class="text-muted f-w-400 form-control" value="2" id="newEmail">
                </div>
                <div class="col-sm-6">
                  <p class="m-b-10 f-w-600">رمز عبور</p>
                  <input type="text" class="text-muted f-w-400 form-control" value="3" id="newPass">
                </div>
              </div>
              <h6 class="m-b-20 m-t-40 p-b-5 b-b-default f-w-600">بیوگرافی</h6>
              <div class="row">
                <div class="col-sm-6">
                  <p class="m-b-10 f-w-600">نام</p>
                  <input type="text" class="text-muted f-w-400 form-control" value="4" id="newFname">
                </div>
                <div class="col-sm-6">
                  <p class="m-b-10 f-w-600">نام خانوادگی</p>
                  <input type="text" class="text-muted f-w-400 form-control" value="5" id="newLname">
                </div>
                <div class="col-sm-12">
                  <p class="m-b-10 f-w-600">شماره همراه</p>
                  <input type="text" class="text-muted f-w-400 form-control" value="6" id="newPhone">
                </div>
                <div class="col-12">
                  <p class="m-b-10 mt-2 f-w-600">آدرس</p>
                  <input type="text" class="text-muted f-w-400 form-control" value="8" id="newAddress">
                </div>
                <div class="col-12 mt-3">
                  <button class="col-12 btn btn-warning" onclick="editProfUser()">ویرایش</button>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>

    <div class="list-cust">
      <table class="table text-light text-center" id="myTable">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">نام</th>
            <th scope="col">نام خانوادگی</th>
            <th scope="col">شماره همراه</th>
            <th scope="col">عملیات</th>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach ($usersInfo as $userInfo) { ?>
            <tr>
              <th scope="row"><?php echo $userInfo["id"] ?></th>
              <td><?php echo $userInfo["name"] ?></td>
              <td><?php echo $userInfo["family"] ?></td>
              <td><?php
                  if ($userInfo["phone"]) {
                    echo $userInfo["phone"];
                  } else {
                    echo "وارد نشده";
                  }
                  ?></td>
              <td>
                <button type="button" class="btn btn-outline-warning mx-1" onclick="showDetails(this);">
                  <i class="fa fa-info"></i>
                </button>
                <button type="button" class="btn btn-outline-danger mx-1" onclick="deleteUser(this);">
                  <i class="fa fa-trash"></i>
                </button>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>

  <!-- jQuery -->
  <script src="assets/js/jquery-3.6.0.min.js"></script>

  <!-- Bootstrap -->
  <script src="assets/js/bootstrap.min.js"></script>

  <!-- Font Awesome -->
  <script defer src="assets/js/all.min.js"></script>

  <!-- Chart JS -->
  <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

  <script src="assets/js/script.js"></script>
</body>

</html>