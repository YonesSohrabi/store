<?php
session_start();
if (!isset($_COOKIE["proID"]) && isset($_POST['item_id'])) {
  setcookie("proID", $_POST['item_id'], time() + 3600);
  exit();
} else if (isset($_POST['item_id'])) {
  setcookie("proID", $_COOKIE["proID"] .= "," . $_POST['item_id']);
  $arrID = explode(",", $_COOKIE['proID']);
  echo count($arrID);
  exit();
}

if (isset($_POST['removeAllCart'])) {
  setcookie("proID", "null", time() - 1);
}

if (isset($_POST['exit'])) {
  setcookie("proID", "null", time() - 1);
  session_destroy();
}

if (isset($_COOKIE["proID"]) && isset($_POST['remove_item_id'])) {
  $arrID = explode(",", $_COOKIE['proID']);
  $newArrID = [];
  foreach ($arrID as $id) {
    if ($id != $_POST['remove_item_id']) {
      $newArrID[] = $id;
    }
  }
  $str = implode(',', $newArrID);
  setcookie("proID", $str);
  echo count($newArrID);
  exit();
}


if (isset($_POST['total_cart_items']) && isset($_COOKIE["proID"])) {
  $arrID = explode(",", $_COOKIE['proID']);
  echo count($arrID);
  exit();
} else {
  echo "0";
}


