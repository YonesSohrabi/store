<?php
class Products {
    private $id;
    private $name;
    private $price;
    private $brand;
    private $imgUrl;
    private $tags;
    private $description;

    public function getId(){
        return $this->id;
    }
    public function setId($id){
        $this->id = $id;
    }

    public function getName(){
        return $this->name;
    }
    public function setName($name){
        $this->name = $name;
    }

    public function getPrice(){
        return $this->price;
    }
    public function setPrice($price){
        $this->price = $price;
    }

    public function getBrand(){
        return $this->brand;
    }
    public function setBrand($brand){
        $this->brand = $brand;
    }

    public function getImgUrl(){
        return $this->imgUrl;
    }
    public function setImgUrl($imgUrl){
        $this->imgUrl = $imgUrl;
    }

    public function getTags(){
        return $this->tags;
    }
    public function setTags($tags){
        $this->tags = $tags;
    }

    public function getDescription(){
        return $this->description;
    }
    public function setDescription($description){
        $this->description = $description;
    }

    public function getInfoProduct()
    {
        require_once "database.php";
        $link = connectToDB();
        $sql = 'SELECT * FROM products WHERE id = :id';
        $statement = $link->prepare($sql);
        $id = $this->getId();
        $statement->bindParam(':id',$id , PDO::PARAM_INT);
        $statement->execute();
        $customer = $statement->fetch(PDO::FETCH_ASSOC);
        if ($customer) {
            $this->setId($customer['id']);
            $this->setName($customer['name']);
            $this->setPrice($customer['price']);
            $this->setBrand($customer['brand']);
            $this->setImgUrl($customer['imgUrl']);
            $this->setTags($customer['tags']);
            $this->setDescription($customer['description']);
            return $customer;
        }
    }

}