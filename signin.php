<?php
if (isset($_COOKIE["blocked"]) ) {
  header("Location: http://localhost/hw/9/block.php");
}
if (!isset($_COOKIE["email"])) {
  setcookie("email", "admin@admin.com", time() + 3600);
  setcookie("password", "123456", time() + 3600);
}
session_start();
if (isset($_SESSION['login'])=="admin") {
  header("Location: http://localhost/hw/9/admin.php");
  die();
}else if(isset($_SESSION['login'])=="user"){
  header("Location: http://localhost/hw/9/index.php");
  die();
}

?>



<!DOCTYPE html>
<html lang="fa" dir="rtl">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Document</title>
  <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
  <link rel="stylesheet" href="assets/css/style.css" />
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" />
  <style>
    body {
      background-color: #535fe6;
      font-weight: 300;
      color: #eee;
    }

    .card {
      border: none;
      padding: 20px;
      background-color: #1c1e21;
      color: #fff;
    }

    .form-input {
      position: relative;
      margin-bottom: 10px;
      margin-top: 10px;
    }

    .form-input i {
      position: absolute;
      font-size: 18px;
      top: 15px;
      left: 10px;
    }

    .form-control,
    .form-select {
      height: 50px;
      background-color: #1c1e21;
      color: darkgray;
      text-indent: 24px;
      font-size: 15px;
    }

    .form-control:focus {
      background-color: #25272a;
      box-shadow: none;
      color: #fff;
      border-color: #4f63e7;
    }

    .form-check-label {
      margin-top: 2px;
      font-size: 14px;
    }

    .signup {
      height: 50px;
      font-size: 14px;
    }
  </style>
</head>

<body>
  <div class="container mt-5 mb-5">
    <div class="row d-flex align-items-center justify-content-center">
      <div class="col-md-6">
        <div class="card px-5 py-5">
          <h5 class="mx-5 text-center fs-2">صفحه ورود</h5>

          <form action="registerController.php" method="post">
            <div class="form-input">
              <input type="text" class="form-control" name="inpEmail" placeholder="آدرس ایمیل" />
            </div>

            <div class="form-input">
              <input type="password" class="form-control" name="inpPass" placeholder="رمز عبور" />
            </div>
            <div class="form-input row d-flex justify-content-between">
              <div class="col-8"><input type="text" class="form-control" name="captcha" placeholder="کد امنیتی را وارد کنید" /></div>
              <?php $randCode = substr(md5(mt_rand()), 0, 5)?>
              <div class="col-4 px-3 py-1 fs-2"><label for="captcha"><?php echo $randCode ?></label></div>
              <?php if(!@$_SESSION["login"]){
                $_SESSION['captchaCode'] = $randCode;
                } ?>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="checkbox" value="YES" name="adminLogin" />
              <label class="form-check-label" for="flexCheckChecked">
                اگر مدیر هستید ، انتخاب کنید
              </label>
            </div>
            <div class="form-input">
              <button type="submit" class="btn btn-primary mt-3 col-12 signup" name="submit">ورود</button>
            </div>
          </form>
          <div class="text-center mt-4">
            <span>اکانت ندارید ؟</span>
            <a href="signup.php" class="text-decoration-none">ثبت نام کنید</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- jQuery -->
  <script src="assets/js/jquery-3.6.0.min.js"></script>

  <!-- Bootstrap -->
  <script src="assets/js/bootstrap.min.js"></script>

  <!-- Font Awesome -->
  <script defer src="assets/js/all.min.js"></script>

  <!-- Chart JS -->
  <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

  <script src="assets/js/script.js"></script>
</body>

</html>