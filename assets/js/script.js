$(document).ready(function () {
  $.ajax({
    type: "post",
    url: "action.php",
    data: {
      total_cart_items: "totalitems",
    },
    success: function (response) {
      $("#total_items").html(response);
    },
  });
});

function cart(id) {
  $.ajax({
    type: "post",
    url: "action.php",
    data: {
      item_id: id,
      total_cart_items: "totalitems",
    },
    success: function (response) {
      $("#total_items").html(response);
    },
  });
}
function removeCart(id) {
  $.ajax({
    type: "post",
    url: "action.php",
    data: {
      remove_item_id: id,
    },
    success: function (response) {
      $("#total_items").html(response);
    },
  });
}

$("#exit").click(() => {
  $.ajax({
    type: "post",
    url: "action.php",
    data: {
      exit: "exit",
    },
    success: function () {
      location.reload();
    },
  });
});
$("#removeAll").click(() => {
  $.ajax({
    type: "post",
    url: "action.php",
    data: {
      removeAllCart: "set",
    },
    success: function () {
      location.reload();
    },
  });
});

function deleteRow(r) {
  const table = document.getElementById("mycart");
  const i = r.parentNode.parentNode.rowIndex;
  table.deleteRow(i-1);
  const id = r.parentNode.parentNode.querySelector("#productID").textContent;
  removeCart(id);
}

// **** SIGN UP **** //

$("#chBox").click(function () {
  if ($(this).prop("checked") == true) {
    $('button[type="submit"]').prop("disabled", false);
  } else {
    $('button[type="submit"]').prop("disabled", true);
  }
});

$("#signup").click(function () {
  let infoUserSignUp = {
    fName : $("#fName").val(),
    lName : $("#lName").val(),
    email : $("#email").val(),
    pass : $("#pass").val(),
  }
  $.ajax({
    type: "post",
    url: "registerController.php",
    data: {
      infoUserSignUp : infoUserSignUp,
    },
    success: function (response) {
      console.log(response);
    },
  });
});

// **** SIGN UP **** //

// ***** ADMIN ***** //

function showDetails(r) {
  const id = r.parentNode.parentNode.firstElementChild.textContent;

  $("#detailsCard").show();
  $.ajax({
    type: "post",
    url: "database.php",
    dataType: "json",
    data: {
      user_info_id: id,
    },
    success: function (response) {
      fillInfoProfile(response);
    },
  });
}

function deleteUser(r) {
  const id = r.parentNode.parentNode.firstElementChild.textContent;
  $.ajax({
    type: "post",
    url: "database.php",
    dataType: "json",
    data: {
      user_del_id: id,
    },
    success: function (response) {
      console.log(1);
    },
  });
}

function fillInfoProfile(data) {
  let lastOnline = null;
  $("#lastOnline").css("color", "green");
  $("div.counter-block").addClass("bg-c-green");
  lastOnline = "دیروز";
  if (!data["phone"]) {data["phone"] = "ثبت نشده";}
  if (!data["address"]) {data["address"] = "ثبت نشده"; }
  $("#fullName").text(data["name"] + " " + data["family"]);
  $("#phoneInCard").text(data["phone"]);
  $("#emailInCard").text(data["email"]);
  $("#lastOnline").text(lastOnline);
  $("#idCustomer").text(data["id"]);
  $("#orderNumUser").text("6");
  $("#orderPriceSum").text("5000");
  $("#customerAddress").text("نشانی : " + data["address"]);
  $("#newEmail").val(data["email"]);
  $("#newPass").val(data["password"]);
  $("#newFname").val(data["name"]);
  $("#newLname").val(data["family"]);
  $("#newPhone").val(data["phone"]);
  $("#newAddress").val(data["address"]);
}

function editProfUser() {
  let updateUser = {
    id : $("#idCustomer").text(),
    email : $("#newEmail").val(),
    password : $("#newPass").val(),
    name : $("#newFname").val(),
    family : $("#newLname").val(),
    phone : $("#newPhone").val(),
    address : $("#newAddress").val()
  }
  $.ajax({
    type: "post",
    url: "database.php",
    dataType: "json",
    data: {
      user_info_upadte: updateUser,
    },  
  });
}

// ***** ADMIN ***** //
