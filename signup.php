<?php 
session_start();
if (isset($_SESSION['login'])=="admin") {
  header("Location: http://localhost/hw/9/admin.php");
  die();
}else if(isset($_SESSION['login'])=="user"){
  header("Location: http://localhost/hw/9/index.php");
  die();
} 
?>



<!DOCTYPE html>
<html lang="fa" dir="rtl">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>ثبت نام</title>
  <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
  <link rel="stylesheet" href="assets/css/style.css" />
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" />
  <style>
    body {
      background-color: #535fe6;
      font-weight: 300;
      color: #eee;
    }

    .card {
      border: none;
      padding: 20px;
      background-color: #1c1e21;
      color: #fff;
    }

    .form-input {
      position: relative;
      margin-bottom: 10px;
      margin-top: 10px;
    }

    .form-input i {
      position: absolute;
      font-size: 18px;
      top: 15px;
      left: 10px;
    }

    .form-control,
    .form-select {
      height: 50px;
      background-color: #1c1e21;
      color: darkgray;
      text-indent: 24px;
      font-size: 15px;
    }

    .form-control:focus {
      background-color: #25272a;
      box-shadow: none;
      color: #fff;
      border-color: #4f63e7;
    }

    .form-check-label {
      margin-top: 2px;
      font-size: 14px;
    }

    .signup {
      height: 50px;
      font-size: 14px;
    }
  </style>
</head>

<body>
  <div class="container mt-5 mb-5">
    <div class="row d-flex align-items-center justify-content-center">
      <div class="col-md-6">
        <div class="card px-5 py-5">
          <h5 class="mx-5 text-center fs-2">ثبت نام کن</h5>

          <div>
            <div class="form-input row d-flex justify-content-between">
              <div class="col-6">
                <input type="text" class="form-control" id="fName" placeholder="نام" />
              </div>
              <div class="col-6">
                <input type="text" class="form-control" id="lName" placeholder="نام خانوادگی" />
              </div>
            </div>
            <div class="form-input">
              <input type="text" class="form-control" id="email" placeholder="آدرس ایمیل" />
            </div>
            <div class="form-input">
              <input type="text" class="form-control" id="pass" placeholder="رمز عبور" />
            </div>
            <div class="form-input">
              <input type="text" class="form-control" id="confPass" placeholder="تائید رمز عبور" />
            </div>
            <div class="form-check">
              <input class="form-check-input" type="checkbox" value="" id="chBox" />
              <label class="form-check-label" for="flexCheckChecked">
                من قوانین را مطالعه کردم و با قوانین کاملا موافقم
              </label>
            </div>
            <button type="submit" class="btn btn-primary signup mt-4 col-12" id="signup" disabled>ثبت نام</button>
          </div>

          <div class="text-center mt-4">
            <span>قبلا ثبت نام کردید ؟</span>
            <a href="signin.php" class="text-decoration-none">ورود</a>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- jQuery -->
  <script src="assets/js/jquery-3.6.0.min.js"></script>

  <!-- Bootstrap -->
  <script src="assets/js/bootstrap.min.js"></script>

  <!-- Font Awesome -->
  <script defer src="assets/js/all.min.js"></script>

  <script src="assets/js/script.js"></script>

</body>

</html>