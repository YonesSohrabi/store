<?php
require_once 'database.php';
include_once 'User.php';
session_start();
function getDataFromSignUP()
{
    $infoUser = $_POST['infoUserSignUp'];
    $user = new User();
    $user->setName($infoUser['fName']);
    $user->setFamily($infoUser['lName']);
    $user->setEmail($infoUser['email']);
    $user->setPassword($infoUser['pass']);
    $user->addUser();
    echo json_encode(true);
}
if (isset($_POST['infoUserSignUp'])) {
    getDataFromSignUP();
}


if (isset($_POST['submit'])) {

    $unicname =  $_POST["inpEmail"] . "_" . "blocklimit";
    if (!isset($_SESSION[$unicname])) {
        $_SESSION[$unicname] = 4;
    }


    if ($_SESSION[$unicname] <= 0) {
        setcookie("blocked", true, time() + 30);
        $_SESSION[$unicname] = 4;
        header("Location: http://localhost/hw/9/block.php");
    } else if (isset($_POST['adminLogin']) && $_POST['adminLogin'] == 'YES') {
        if (
            ($_POST['inpEmail'] == $_COOKIE['email']) &&
            ($_POST['inpPass'] == $_COOKIE['password']) &&
            $_SESSION['captchaCode'] == trim($_POST['captcha'])
        ) {
            $_SESSION["login"] = "admin";
            setcookie("timeLogin", time());
            $_SESSION[$unicname] = 4;
            header("Location: http://localhost/hw/9/admin.php");
        } else {
            $_SESSION[$unicname]--;
            header("Location: http://localhost/hw/9/signin.php");
            die();
        }
    } else {
        require_once "database.php";
        $users = getAllUser();
        $isLogged = false;
        var_dump($users);
        foreach ($users as $user) {
            if (
                ($_POST['inpEmail'] == $user['email']) &&
                ($_POST['inpPass'] == $user['password']) &&
                $_SESSION['captchaCode'] == trim($_POST['captcha'])
            ) {
                $_SESSION["login"] = "user";
                $_SESSION["userID"] = $user['id'];
                setcookie("timeLogin", time());
                $_SESSION[$unicname] = 4;
                $isLogged = true;
                header("Location: http://localhost/hw/9/index.php");
            }
        }
        if (!$isLogged) {
            $_SESSION[$unicname]--;
            header("Location: http://localhost/hw/9/signin.php");
            die();
        }
    }
}
